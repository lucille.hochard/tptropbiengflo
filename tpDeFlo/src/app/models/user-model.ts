export class UserModel {
    id: number;
    url: String;
    name: String;
    username: String;
    email: String;

    constructor(id:number, url:string, name:string, email:string, username:string) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.username = username;
        this.email = email;
    }

    
}
